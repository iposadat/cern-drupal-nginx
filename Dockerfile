FROM nginx:stable

# Set consistent timezone
ENV CONTAINER_TIMEZONE="UTC"
RUN rm -f /etc/localtime \
 && ln -s /usr/share/zoneinfo/${CONTAINER_TIMEZONE} /etc/localtime

# Install prerequisite OS packages
RUN apt-get update -y && apt-get upgrade -y && apt-get install -y curl

RUN rm /etc/nginx/conf.d/default.conf

COPY fastcgi.conf /etc/nginx/conf.d/

RUN mkdir -p "/var/cache/nginx/" && \
    chmod a+rw "/var/cache/nginx/" && \
    chmod a+rw "/var/run/"

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]